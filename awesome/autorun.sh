#!/usr/bin/env bash

# Define run function
function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

# Startup applications list
run picom -b
run nitrogen --restore
run xscreensaver -no-splash
run flameshot
run nm-applet
run numlockx on
