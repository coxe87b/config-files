#!/usr/bin/env bash

# Terminate existing
killall -q polybar

# Wait
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch
polybar -q main &
