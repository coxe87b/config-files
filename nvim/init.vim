" Plugins ----- {{{
call plug#begin('~/.config/nvim/plugged')
    Plug 'vifm/vifm.vim'
    Plug 'dracula/vim', {'as':'dracula'}
    Plug 'vim-scripts/delimitMate.vim'
call plug#end()
" }}}

" General ----- {{{
set encoding=UTF-8
set nocompatible
set wrap
set linebreak
filetype on
filetype plugin on
filetype indent on
syntax on
syntax enable
set shiftwidth=4 tabstop=4 expandtab autoindent smarttab
set nowrap incsearch smartcase
set showcmd showmode showmatch hlsearch
set history=1000 spell
set wildmenu wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.exe,*.img,*xlsx
set shell=/usr/bin/zsh
:set rnu
:set nu
" }}}

" Status line ----- {{{
set statusline=
set statusline+=%#Search#
set statusline+=\ %y
set statusline+=\ %r
set statusline+=%#Visual#
set statusline+=\ %F
set statusline+=%= "Right side
set statusline+=%#ErrorMsg#
set statusline+=\ %p%%
set statusline+=\ [%l/%L,
set statusline+=%c]
" }}}

" Key mapping ----- {{{
:imap ii <Esc>
map <F1> :set invnu invrnu <CR>
" }}}

" Colour Settings ----- {{{
set background=dark
set termguicolors
set cursorline
"set cursorcolumn
colorscheme dracula

" }}}

" Enable line folding by the following:
" augroup filetype_vim
"     autocmd!
"     autocmd FileType vim setlocal foldmethod=marker
" augroup END

