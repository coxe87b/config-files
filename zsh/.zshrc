HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -v  #vi mode
zstyle :compinstall filename '/home/ewan/.config/zsh/.zshrc'

autoload -Uz compinit
compinit

autoload -Uz promptinit
promptinit

# Variables
export VISUAL="/usr/bin/nvim"
export EDITOR="$VISUAL"
fpath=("$HOME/.zprompts" "$fpath[@]")
export XDG_CONFIG_HOME="$HOME/.config"
export CONF_DIR=$XDG_CONFIG_HOME

# Source aliases
source $CONF_DIR/.aliases

# Prompt
# PS1=' /usr/bin/zsh> ∮ '
PROMPT=' %F{blue}%~ %F{yellow}∮%f '

# Execute
neofetch

source $CONF_DIR/zsh/zsh-fs-free.sh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
