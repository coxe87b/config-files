#!/bin/zsh
#
# Script to display filesystem usage snapshot
#
echo

df -h / | awk 'NR==2 {print "Root filesystem - [" $1 "] mounted on [/] ------------ " $5 " in use - " $4 " available"}'  # ; if [[ $(df -h / | awk 'NR==2 {print $4}') -lt "20" ]]; then; echo "below 20"; fi
df -h /home | awk 'NR==2 {print "Filesystem ------ [" $1 "] mounted on [/home] -------- " $5 " in use - " $4 " available"}'
df -h ~/Downloads | awk 'NR==2 {print "Filesystem ------ [" $1 "] mounted on [~/Downloads] ------- " $5 " in use - " $4 " available"}'
df -h /mnt/wdblue-2tb | awk 'NR==2 {print "Filesystem ------ [" $1 "] mounted on [/mnt/wdblue-2tb] --- " $5 " in use - " $4 " available"}'
df -h /mnt/nvme500 | awk 'NR==2 {print "Games disk ------ [" $1 "] mounted on [/mnt/nvme500] - " $5 " in use - " $4 " available"}'

echo
